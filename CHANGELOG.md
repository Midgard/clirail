# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/).
Since this is not a library, this project does not adhere to
[Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]
### Added
### Changed
### Deprecated
### Removed
### Fixed
### Security

## [1.7.3] - 2023-08-02
### Added
### Changed
- Make train type detection more robust against future train types
- Add better error handling when iRail could not respond to our request

### Deprecated
### Removed
### Fixed
### Security

## [1.7.2] - 2023-08-02
### Added
- Ignore the alert about coastal express
- Recognize the T (tourist train) and EXP (coastal express) train types

### Changed
### Deprecated
### Removed
### Fixed
### Security

## [1.7.1] - 2022-11-28
### Fixed
- Add main script so source builds work (it had been accidentally gitignored)

## [1.7] - 2022-05-08
### Changed
- Update message to ignore about face masks

## [1.6] - 2021-07-17
### Added
- Show date in results when not today

## [1.5] - 2021-07-06
### Added
- Ignore the alert about masks being compulsory

## [1.4] - 2020-10-31
### Added
- Emoji for Eurostar

## [1.3] - 2020-06-29
### Added
- Change log

### Fixed
- Make all strings translatable

## [1.2] - 2020-05-18
### Fixed
- Improve help text and documentation

## [1.1] - 2020-05-17
### Added
- Help text

### Fixed
- Correct `create_env.sh` script

## [1.0] - 2020-05-17
### Added
- Route planning, liveboards and timeliness overview
- Published on PyPI
