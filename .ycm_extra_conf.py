# Configuration for YouCompleteMe, a Vim plugin: https://github.com/ycm-core/YouCompleteMe

def Settings(**kwargs):
	return {
		"interpreter_path": "./venv/bin/python"
	}
